import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Mickey from './components/Mickey';

function App() {
  return (
    <div className="App">
      <Header/>
      <Mickey/>
      <Body/>
      <Footer/>
    </div>
  );
}

export default App;
