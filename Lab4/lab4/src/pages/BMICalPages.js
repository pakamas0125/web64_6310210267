import BMIResult from "../components/BMIResult";
import {useState} from"react";

function BMICalPages(){

    const [name,setName] = useState("");
    const [bmiResult,setBmiResult] = useState("");
    const [translateResult,settranslateResut] 
                                            = useState("");
    const [height,setHeight] = useState("");
    const [weight,setWeight] = useState("");
    
    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h * h);
        setBmiResult( bmi );
        if(bmi > 25){
            settranslateResut("อ้วนแน้ว");
        
        }else {
            settranslateResut("ผอมแว้ว");
        }
    }

    return (
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr/>
                คุณชื่อ: <input type="text" 
                                 value={name}
                                 onChange={(e) => {setName(e.target.value);}}/><br/>
                ส่วนสูง: <input type="text"
                                value={height}
                                onChange={(e) => {setHeight(e.target.value);}}/><br/>
                น้ำหนัก: <input type="text"
                                value={weight}
                                onChange={(e) => {setWeight(e.target.value);}}/><br/>
                                

                <button onClick={(e) => (calculateBMI())}> calculate</button>
                { bmiResult != 0 &&
                <div>
                <hr/>
                นี่ผลการคำนวณค่ะ

               
                <BMIResult
                name={name} 
                bmi = {bmiResult}
                result = {translateResult}
                />
                </div>
                }
                 </div>
   </div>

    );
}

export default BMICalPages;

