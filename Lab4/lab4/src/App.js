import logo from './logo.svg';
import './App.css';

import AboutUsPages from './pages/AboutUsPages';
import BMICalPages from './pages/BMICalPages';
import Header from './components/Header';

import {Routes,Route} from "react-router-dom";
import LuckyNumberPage from './pages/LuckyNumberPage';

function App() {
  return (
    <div className="App">
      <Header />
        <Routes>

          <Route path="about" element={
                  <AboutUsPages/>
                  }/>
          <Route path="/" element={
            <BMICalPages/>
            }/>
          <Route path="lucky" element={
            <LuckyNumberPage/>
            }/>
        
        </Routes>
    </div>
  );
}

export default App;
